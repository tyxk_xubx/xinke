package com.tyxk.xinke.daemon.quartz;

import com.tyxk.xinke.common.feign.annotation.EnableXinkeFeignClients;
import com.tyxk.xinke.common.security.annotation.EnableXinkeResourceServer;
import com.tyxk.xinke.common.swagger.annotation.EnableXinkeSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author frwcloud
 * @date 2019/01/23
 * 定时任务模块
 */
@EnableXinkeSwagger2
@EnableXinkeFeignClients
@SpringCloudApplication
@EnableXinkeResourceServer
public class XinkeDaemonQuartzApplication {

	public static void main(String[] args) {
		SpringApplication.run(XinkeDaemonQuartzApplication.class, args);
	}
}
