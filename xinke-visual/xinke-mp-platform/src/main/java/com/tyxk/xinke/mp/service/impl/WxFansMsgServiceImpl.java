/*
 *    Copyright (c) 2018-2025, xinke All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: xinke
 */
package com.tyxk.xinke.mp.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tyxk.xinke.mp.entity.WxFansMsg;
import com.tyxk.xinke.mp.mapper.WxFansMsgMapper;
import com.tyxk.xinke.mp.service.WxFansMsgService;
import org.springframework.stereotype.Service;

/**
 * 微信消息业务
 *
 * @author xinke
 * @date 2019-03-27 20:45:27
 */
@Service
public class WxFansMsgServiceImpl extends ServiceImpl<WxFansMsgMapper, WxFansMsg> implements WxFansMsgService {

}
