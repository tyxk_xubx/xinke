package com.tyxk.xinke.common.sequence.range;

/**
 * @author xinke
 * @date 2019-05-26
 * <p>
 * 名称生成器
 */
public interface BizName {
	/**
	 * 生成名称
	 */
	String create();

}
