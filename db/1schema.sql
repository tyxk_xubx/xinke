-- xinke 核心表
create database `xinkex` default character set utf8mb4 collate utf8mb4_general_ci;

-- xinke 工作流相关库
create database `xinkex_ac` default character set utf8mb4 collate utf8mb4_general_ci;

-- xinke 任务相关库
create database `xinkex_job` default character set utf8mb4 collate utf8mb4_general_ci;

-- xinke 公众号管理相关库
create database `xinkex_mp` default character set utf8mb4 collate utf8mb4_general_ci;

-- xinke nacos配置相关库
create database `xinkex_config` default character set utf8mb4 collate utf8mb4_general_ci;

-- xinke pay配置相关库
create database `xinkex_pay` default character set utf8mb4 collate utf8mb4_general_ci;

-- xinke codegen相关库
create database `xinkex_codegen` default character set utf8mb4 collate utf8mb4_general_ci;